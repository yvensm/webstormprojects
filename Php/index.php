<html>
<head>
    <meta charset="UTF-8">
    <title>Cadastro de Produtos</title>
    <link rel="stylesheet" href="_css/estilo.css">

</head>
<body>
    <div class="container">
        <div class="cadastro">
            <form action="/form.php" method="get">
                Nome: <br>
                <input type="text" name="nome" class="nome" ><br>
                Categoria:<br>
                <input type="number" class="categoria"><br>
                Preço:<br>
                <input type="number" class="valor"><br>
                Descrição: <br>
                <input type="text" class="descricao"><br>
                <input type="checkbox" class="cardapio">
                Deseja que este produto apareça no cardapio digital?<br>
                <br>
                <input type="checkbox" class="delivery">
                Deseja que este produto esteja disponivel para Delivery?<br>
                <br>
                <input type="submit" class="salvar">
            </form>
        </div>
    </div>
</body>
</html>

