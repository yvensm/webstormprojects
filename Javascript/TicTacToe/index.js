//Matriz de jogo
var matriz = [
    0,0,0,
    0,0,0,
    0,0,0
];

//Jogador Atual
var player= 'X';
var xNextPlayer = true;
//Vencedor
var win=false;
//Matriz de ações: Primeiro vetor é de jogadas feitas
//Segundo vetor de jogadas desfeitas
var action=[[],[]];


//Ação do botão
function gameButton(x) {

    var button = document.getElementById("button"+x);
    var textPlayer = document.getElementById('status');
    //Só aceita novas jogadas se não possuir vencedor e o botão não tiver sido pressionado antes
    if(!win && matriz[x] === 0){
        //Adiciona o numero do botao que foi pressionado no vetor de acoes
        action[0].push(x);
        //Se foi feito uma nova jogada, limpar o vetor de refazer acoes
        action[1]=[];
        //Adicionar o valor do jogador a matriz
        matriz[x] = player;
        //Reprint Botao
        button.innerHTML = player;
        //Trocar jogador
        xNextPlayer = !xNextPlayer;
        player = xNextPlayer ? 'X' : 'O';
        //Checar Vencedor
        win = checkWinner();
        //Reprint no texto de jogador se não houve vencedor
        if(!win) {
            textPlayer.innerHTML = "Próximo jogador: " + player;
        }

    }

}
//Checar Vencedor
function checkWinner() {
    //Matriz de possibilidades de vitória
    var winOptions = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [2,4,6],
        [0,4,8]
    ];
    //Pegando o Status
    var status = document.getElementById('status');
    //Checando vencedor
    //Para cada linha na matriz de possibilidades
    for(let i = 0; i<winOptions.length;i++){
        //a,b,c = aos respectivos numeros na linha da matriz
        const[a,b,c]= winOptions[i];
        //Se o Primeiro and Primeiro igual segundo And primeiro igual terceiro logo todos são iguais;
        if(matriz[a] && matriz[a] === matriz[b] && matriz[a] === matriz[c]){
            status.innerHTML = "Vencedor: " + matriz[a];
            return true;
        }
    }

    for(var i = 0; i<9;i++){
        if(matriz[i]===0){
            break;
        }
    }
    if(i === 9){
        status.innerHTML = "Deu Velha!!!";
        return true;
    }
    return false;
}

function refreshGame() {
    //Restart
    //Alerta de Restart
    if(confirm("Deseja reiniciar o Jogo?")) {
        //Valores padroes
        matriz = [
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        ];
        win = false;
        action=[[],[]];
        xNextPlayer = true;
        document.getElementById('status').innerHTML = "Próximo jogador: X";
        player = 'X';
        //Reprint de todos os botoes
        for (let i = 0; i < 9; i++) {
            var button = document.getElementById("button" + i);
            button.innerHTML = '-';
        }
    }
}

function undoAction() {

    //Desfazer jogada apenas se ja existir alguma jogada
    if(action[0][action[0].length-1] !== undefined) {
        //Remover a ultima jogada e adicionar no vetor de refazer
        var rm = action[0].pop();
        action[1].push(rm);

        //Colocar o valor padrão inicial na posição da ultima jogada
        matriz[rm] = 0;
        //Limpar o botão
        document.getElementById('button' + rm).innerHTML = '-';
        //Voltar pro player anterior
        xNextPlayer = !xNextPlayer;
        player = xNextPlayer ? "X" : "O";
        document.getElementById('status').innerHTML = "Próximo jogador: " + player;
        //Desfazer vitória se alguem tiver ganhado
        win=false;
    }
}


function redoAction() {
    //Refazer jogada se existis jogada desfeita
    if(action[1][action[1].length-1] !== undefined) {
        //Remove do vetor de refazer e adiciona novamente no vetor de jogadas
        var rd = action[1].pop();
        action[0].push(rd);
        //Recoloca o valor na matrix
        matriz[rd] = player;
        //Reprint no botão
        document.getElementById('button' + rd).innerHTML = player;
        //Autera pro próximo jogador;
        xNextPlayer = !xNextPlayer;
        player = xNextPlayer ? "X" : "O";
        document.getElementById('status').innerHTML = "Próximo jogador: " + player;
        //Checa se a ultima jogada foi a da vitória
        win = checkWinner();
    }
}

function saveGame() {
    if (confirm('Deseja Salvar este jogo? Isso irá sobrescrever o jogo salvo anteriormente.')){
        if (typeof (Storage) !== "undefined") {
            window.localStorage.setItem('matriz', matriz.toString());
            window.localStorage.setItem('player', xNextPlayer.toString());
        } else {
            alert("Seu navegador não suporta esta função")
        }
    }
}

function loadGame() {
    if(confirm("Deseja Carregar o ultimo jogo salvo?")) {
        var mat = window.localStorage.getItem('matriz');
        var p = window.localStorage.getItem('player');
        console.log(p);
        for (let i = 0; i < mat.length; i += 2) {

            console.log(mat[i]);
            if (mat[i] !== '0' && mat[i] !== ',') {
                matriz[i / 2] = mat[i];
                document.getElementById('button' + i / 2).innerHTML = mat[i];
            } else {
                matriz[i / 2] = 0;
                document.getElementById('button' + i / 2).innerHTML = '-';
            }
        }
        player = p === 'true' ? 'X' : 'O';
        xNextPlayer = p === 'true';
        document.getElementById('status').innerHTML = 'Próximo jogador: ' + (p === 'true' ? 'X' : 'O');
        action = [[], []];
        win = checkWinner();
    }
}