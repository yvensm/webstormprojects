<!Doctype html>

<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <meta	name="viewport"	content="width=device-width">

    <title>Mirror Fashion</title>
    <link	rel="stylesheet" href="_css/reset.css">
    <link rel="stylesheet" href="_css/estilos.css">
    <link rel="stylesheet" href="_css/mobile.css" media="(max-width: 939px)">


</head>
<body>


<header class="container">
    <!-- cabeçalho -->
    <h1><img src="_imagens/logo.png" alt="Mirror Fashion"/></h1>

    <p class="sacola">Nenhum item na sacola de compras.</p>

    <nav class="menu-opcoes">
        <ul>
            <li><a	href="#">Sua	Conta</a></li>
            <li><a	href="#">Lista	de	Desejos</a></li>
            <li><a	href="#">Cartão	Fidelidade</a></li>
            <li><a	href="sobre.html">Sobre</a></li>
            <li><a	href="#">Ajuda</a></li>
        </ul>
    </nav>
</header>

<div class="container destaque">
    <section class="busca">
        <h2>Busca</h2>
        <form>

            <input type="search" title="Busca">
            <input type="image" src="_imagens/busca.png" class="lupa"/>
        </form>
    </section>

    <section class="menu-departamentos">
        <h2>Departamentos</h2>
        <nav>
            <ul>
                <li><a	href="#">Blusas	e	Camisas</a>
                    <ul>
                        <li><a	href="#">Manga	curta</a></li>
                        <li><a	href="#">Manga	comprida</a></li>
                        <li><a	href="#">Camisa	social</a></li>
                        <li><a	href="#">Camisa	casual</a></li>
                    </ul>
                </li>
                <li><a	href="#">Calças</a></li>
                <li><a	href="#">Saias</a></li>
                <li><a	href="#">Vestidos</a></li>
                <li><a	href="#">Sapatos</a></li>
                <li><a	href="#">Bolsas	e	Carteiras</a></li>
                <li><a	href="#">Acessórios</a></li>
            </ul>
        </nav>
    </section><!--	fim	.menu-departamentos	-->
    <img	src="_imagens/destaque-home.png"	alt="Promoção: Big City Night">
</div>

<div class="container paineis">

    <section class="painel novidades">
        <h2>Novidades</h2>
        <ol>

            <li>
                <a href="produto.html">
                    <figure>
                        <img src="_imagens/produtos/miniatura1.png"/>
                        <figcaption>Fuzz Cardigan por R$129,90</figcaption>
                    </figure>
                </a>
            </li>

            <li>
                <a href="produto.html">
                    <figure>
                        <img src="_imagens/produtos/miniatura2.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>

            <li>
                <a href="proroduto.html">
                    <figure>
                        <img src="_imagens/produtos/miniatura3.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>

            <li>
                <a href="#">
                    <figure>
                        <img src="_imagens/produtos/miniatura4.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>

            <li>
                <a href="#">
                    <figure>
                        <img src="_imagens/produtos/miniatura5.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>

        </ol>

    </section>


    <section class="painel mais-vendidos">
        <h2>Mais Vendidos</h2>
        <ol>
            <li >
                <a href="produto.php">
                    <figure>
                        <img src="_imagens/produtos/miniatura1.png"/>
                        <figcaption>Fuzz Cardigan por R$129,90</figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="produto.php">
                    <figure>
                        <img src="_imagens/produtos/miniatura3.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="produto.html">
                    <figure>
                        <img src="_imagens/produtos/miniatura4.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>
            <li>
                <a href="produto.html">
                    <figure>
                        <img src="_imagens/produtos/miniatura5.png"/>
                        <figcaption>Camiseta Bad por R$39,90</figcaption>
                    </figure>
                </a>
            </li>
            <!-- produtos mais vendidos aqui -->
        </ol>
    </section>
</div>

<footer>
    <div class="container">
        <img src="_imagens/logo-rodape.png"	alt="Logo	Mirror	Fashion">

        <ul	class="social">
            <li><a	href="http://facebook.com/mirrorfashion">Facebook</a></li>
            <li><a	href="http://twitter.com/mirrorfashion">Twitter</a></li>
            <li><a	href="http://plus.google.com/mirrorfashion">Google+</a></li>
        </ul>
    </div>
    <!-- rodape -->
</footer>

<script src="js/home.js"></script>
</body>
</html>