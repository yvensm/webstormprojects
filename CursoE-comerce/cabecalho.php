<html>

    <head>
        <meta	charset="utf-8">
        <title><?php	print	$cabecalho_title;	?></title>
        <link	rel="stylesheet"    type="text/css"	href="_css/reset.css">
        <link	rel="stylesheet" type="text/css" href="_css/estilos.css">
        <link	rel="stylesheet" type="text/css"	href="_css/mobile.css"	media="(max-width:	939px)">
        <meta	name="viewport"	content="width=device-width">
        <?php	print	@$cabecalho_css;	?>
    </head>



    <body>
    <header	class="container">
        <h1>
            <img	src="_imagens/logo.png"	alt="Mirror	Fashion">
        </h1>
        <p	class="sacola">
            Nenhum	item	na	sacola	de	compras
        </p>
        <nav	class="menu-opcoes">
            <ul>
                <li><a	href="#">Sua	Conta</a></li>
                <li><a	href="#">Lista	de	Desejos</a></li>
                <li><a	href="#">Cartão	Fidelidade</a></li>
                <li><a	href="sobre.php">Sobre</a></li>
                <li><a	href="#">Ajuda</a></li>
            </ul>
        </nav>
    </header>
    </body>
</html>