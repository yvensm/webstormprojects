var $input_quantidade = document.querySelector("#qt");
var $output_total = document.querySelector("#total");

function newPreco() {
    var preco = document.querySelector("#preco").textContent;
    preco = preco.replace("R$ ","");
    preco = preco.replace(",",".");
    console.log(preco);
    preco = parseFloat(preco);

    var quantidade = $input_quantidade.value;
    var total = quantidade * preco;

    total = "R$ " + total.toFixed(2);
    total = total.replace(".",",");

    $output_total.value = total;
}

$input_quantidade.oninput = newPreco