<!doctype <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Checkout Mirror Fashion</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--
    <style>
        .form-control:invalid	{
            border:	1px	solid	#cc0000;.
        }
    </style>
 -->
    <style>
        .navbar {
            margin: 0 !important;
        }
        body	{	padding-top:	50px;	}

        .navbar img{
            padding: 5px;
            margin: 5px;
            height: 40px;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>

<body>


    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <button	type="button"	class="navbar-toggle	collapsed"
                       data-toggle="collapse"	data-target="#navbar-collapse-id">
                <span	class="icon-bar"></span>
                <span	class="icon-bar"></span>
                <span	class="icon-bar"></span>
            </button>
            <img src="_imagens/logo-rodape.png">
            <!--<a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span> Mirror Fashion</a>-->
        </div>

        <div id="navbar-collapse-id" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="sobre.php"><span class="glyphicon glyphicon-list-alt"></span> Sobre</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span> Ajuda</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-question-sign"></span> Perguntas Frequentes</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-bullhorn"></span> Entre em Contato</a></li>
            </ul>
        </div>
    </nav>

    <div class="jumbotron">
        <div id="cabeca" class="container">
            <h1>Ótima Escolha</h1>
            <p>Obrigrado por comprar na Mirror Fashion! Preencha seus dados para efetivar a compra.</p>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!--Panel-->
            <div class="col-sm-4 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Sua compra</h2>
                    </div>
                    <div class="panel-body">
                        <img src="_imagens/produtos/foto1-<?=$_POST['cor']?>.png" class="img-thumbnail img-responsive hidden-xs">
                        <dl>
                            <dt>Nome</dt>
                            <dd>
                                <?= $_POST['nome'] ?>
                            </dd>

                            <dt>Preço</dt>
                            <dd id="preco">
                                R$ <?= $_POST['preco'] ?>
                            </dd>

                            <dt>Cor</dt>
                            <dd>
                                <?= $_POST['cor'] ?>
                            </dd>
                            <dt>Tamanho</dt>
                            <dd>
                                <?= $_POST['tamanho'] ?>
                            </dd>
                        </dl>
                        <div class="form-group">
                            <label for="qt">Quantidade</label>
                            <input id="qt" for="qt" class="form-control" type="number" min="0", max="99" value="1">
                        </div>
                        <div class="form-group">
                            <label for="total">Total</label>
                            <output for="qt preco" id="total" class="form-control">
                                <?= $_POST["preco"] ?>
                            </output>
                        </div>
                    </div>
                </div>
            </div>

            <!--Formulario-->
            <form class="col-sm-8 col-lg-9">
                <div class="row">
                    <fieldset class="col-md-6">

                        <legend>Dados Pessoais</legend>
                        <div class="form-group">
                            <label for="name">Nome completo</label>
                            <input type="text" class="form-control" id="name" name="name" autofocus required>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <span class="input-group-addon">@</span>
                                <input type="email" class="form-control" id="email" name="email" placeholder="email@exemplo.com">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" id="cpf" name="cpf" placeholder="000.000.000-00" required>
                        </div>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="sim" name="spam" checked>
                                Quero receber spam da Mirror Fashion
                            </label>
                        </div>

                    </fieldset>

                    <fieldset class="col-md-6">
                        <legend>Cartão de crédito</legend>

                        <div class="form-group">
                            <label for="numero-cartao">Número - CVV</label>
                            <input type="text" class="form-control" id="numero-cartao" name="numero-cartao">
                        </div>

                        <div class="form-group">
                            <label for="bandeira-cartao">Bandeira</label>
                            <select name="bandeira-cartao" id="bandeira-cartao" class="form-control">
                                <option value="master">MasterCard</option>
                                <option value="visa">Visa</option>
                                <option value="amex">American Express</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="validade-cartao">Validade</label>
                            <input type="month" class="form-control" id="validade-cartao" name="validade-cartao">
                    </fieldset class>
                </div>

                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-thumbs-up"></span>
                    Confirmar Pedido
                </button>
            </form>

        </div>

    </div>

    <script	src="js/total.js"></script>
    <script	src="js/jquery.js"></script>
    <script	src="js/bootstrap.js"></script>

</body>

</html>