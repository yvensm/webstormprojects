<<<<<<< HEAD:CursoE-comerce/sobre.html
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Sobre a Mirror Fashion</title>
    <link rel="stylesheet" href="_css/estilo.css"/>

</head>
<body>
    <img src="_imagens/logo.png"/>

    <h1>A Mirror Fashion</h1>

    <p>A	<strong>Mirror	Fashion</strong>	é	a	maior	empresa	de	comércio	eletrônico	no	segmento
        de	moda	em	todo	o	mundo.	Fundada	em	1932,	possui	filiais	em	124	países,	sendo	líder	de	mercado	com	mais	de	90%	de
        participação	em	118	deles.</p>

    <p>
        Nosso	centro	de	distribuição	fica	em	Jacarezinho,	no	Paraná.	De	lá,	saem	48	aviões	que
        distribuem	nossos	produtos	às	casas	do	mundo	todo.	Nosso	centro	de	distribuição:
    </p>

    <figure id="centro-distribuicao">
        <img src="_imagens/ctd-mf.png"/>
        <figcaption>Centro de Distribuição Mirror Fashion</figcaption>
    </figure>


    <p>
        Compre	suas	roupas	e	acessórios	na	Mirror	Fashion.	<a href="index.html">Acesse	nossa</a>	loja	ou	entre	em	contato
        se	tiver	dúvidas.	Conheça	também	nossa	<a href="#historia">história</a>	e	nossos	<a href="#diferenciais">diferenciais</a>.
    </p>

    <h2 id="historia">História</h2>

    <figure id="paelho-family">
        <img src="_imagens/paelhofamily.png"/>
        <figcaption>Familia Paelho</figcaption>
    </figure>

    <p>
        A	fundação	em	1932	ocorreu	no	momento	da	descoberta	econônica	do	interior	do	Paraná.	A
        família	Pelho,	tradicional	da	região,	investiu	todas	as	suas	economias	nessa	nova	iniciativ
        a,
        revolucionária	para	a	época.	O	fundador	<em>Eduardo	Simões	Pelho</em>,	dotado	de	particular
        visão
        administrativa,	guiou	os	negócios	da	empresa	durante	mais	de	50	anos,	muitos	deles	ao	lado
        de	seu	filho	<em>E.	S.	Pelho	Filho</em>,	atual	CEO.	O	nome	da	empresa	é	inspirado	no	nome	d
        a	família.
    </p>
    <p>
        O	crescimento	da	empresa	foi	praticamente	instantâneo.	Nos	primeiros	5	anos,	já	atendia	18
        países.
        Bateu	a	marca	de	100	países	em	apenas	15	anos	de	existência.	Até	hoje,	já	atendeu	740	milhõ
        es
        de	usuários	diferentes,	em	bilhões	de	diferentes	pedidos.
    </p>
    <p>
        O	crescimento	em	número	de	funcionários	é	também	assombroso.	Hoje,	é	a	maior	empregadora	do

        Brasil,	mas	mesmo	após	apenas	5	anos	de	sua	existência,	já	possuía	30	mil	funcionários.	For
        a	do
        Brasil,	há	240	mil	funcionários,	além	dos	890	mil	brasileiros	nas	instalações	de	Jacarezinh
        o	e
        nos	escritórios	em	todo	país.
    </p>
    <p>
        Dada	a	importância	econômica	da	empresa	para	o	Brasil,	a	família	Pelho	já	recebeu	diversos
        prêmios,
        homenagens	e	condecorações.	Todos	os	presidentes	do	Brasil	já	visitaram	as	instalações	da	M
        irror
        Fashion,	além	de	presidentes	da	União	Européia,	Ásia	e	o	secretário-geral	da	ONU.
    </p>


    <h2 id="diferenciais">Diferenciais</h2>
    <ul>
        <li> Menor preço do varejo, garantido</li>
        <li>Se você achar uma loja mais barata, leva o produto de graça</li>
        <li>Pague em reais, dólares, euros ou bitcoins</li>
        <li>Todas as compras com frete grátis para o mundo todo</li>
        <li>Maior comércio eletrônico de moda do mundo</li>
        <li>Atendimento via telefone, email, chat, twitter, facebook, carta, fax e telegrama</li>
        <li>Presente em 124 países</li>
        <li>Mais de um milhão de funcionários em todo o mundo</li>
    </ul>

    <div id="rodape">
        <img src="_imagens/logo.png">
        &copy; copyright Mirror Fashion
    </div>
</body>
=======
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Sobre a Mirror Fashion</title>
    <link rel="stylesheet" href="_css/sobre.css"/>

</head>
<body>
    <div id="cabecalho">
        <h1 class="logo"><img src="_imagens/logo.png" alt="Mirror Fashion"/></h1>

        <p class="sacola">Nenhum item na sacola de compras.</p>

        <nav class="menu-opcoes">
            <ul>
                <li><a	href="#">Sua	Conta</a></li>
                <li><a	href="#">Lista	de	Desejos</a></li>
                <li><a	href="#">Cartão	Fidelidade</a></li>
                <li><a	href="sobre.php">Sobre</a></li>
                <li><a	href="#">Ajuda</a></li>
            </ul>
        </nav>
    </div>
    <h1 class="titulo">A Mirror Fashion</h1>

    <p>A	<strong>Mirror	Fashion</strong>	é	a	maior	empresa	de	comércio	eletrônico	no	segmento
        de	moda	em	todo	o	mundo.	Fundada	há <?php	print	date("Y") - 1932; ?> anos, possui	filiais	em	124	países,	sendo	líder	de	mercado	com	mais	de	90%	de
        participação	em	118	deles.</p>

    <p>
        Nosso	centro	de	distribuição	fica	em	Jacarezinho,	no	Paraná.	De	lá,	saem	48	aviões	que
        distribuem	nossos	produtos	às	casas	do	mundo	todo.	Nosso	centro	de	distribuição:
    </p>

    <figure id="centro-distribuicao">
        <img src="_imagens/ctd-mf.png"/>
        <figcaption>Centro de Distribuição Mirror Fashion</figcaption>
    </figure>


    <p>
        Compre	suas	roupas	e	acessórios	na	Mirror	Fashion.	<a href="index.php">Acesse	nossa</a>	loja	ou	entre	em	contato
        se	tiver	dúvidas.	Conheça	também	nossa	<a href="#historia">história</a>	e	nossos	<a href="#diferenciais">diferenciais</a>.
    </p>

    <h2 id="historia">História</h2>

    <figure id="paelho-family">
        <img src="_imagens/paelhofamily.png"/>
        <figcaption>Familia Paelho</figcaption>
    </figure>

    <p>
        A	fundação	em	1932	ocorreu	no	momento	da	descoberta	econônica	do	interior	do	Paraná.	A
        família	Pelho,	tradicional	da	região,	investiu	todas	as	suas	economias	nessa	nova	iniciativ
        a,
        revolucionária	para	a	época.	O	fundador	<em>Eduardo	Simões	Pelho</em>,	dotado	de	particular
        visão
        administrativa,	guiou	os	negócios	da	empresa	durante	mais	de	50	anos,	muitos	deles	ao	lado
        de	seu	filho	<em>E.	S.	Pelho	Filho</em>,	atual	CEO.	O	nome	da	empresa	é	inspirado	no	nome	d
        a	família.
    </p>
    <p>
        O	crescimento	da	empresa	foi	praticamente	instantâneo.	Nos	primeiros	5	anos,	já	atendia	18
        países.
        Bateu	a	marca	de	100	países	em	apenas	15	anos	de	existência.	Até	hoje,	já	atendeu	740	milhõ
        es
        de	usuários	diferentes,	em	bilhões	de	diferentes	pedidos.
    </p>
    <p>
        O	crescimento	em	número	de	funcionários	é	também	assombroso.	Hoje,	é	a	maior	empregadora	do

        Brasil,	mas	mesmo	após	apenas	5	anos	de	sua	existência,	já	possuía	30	mil	funcionários.	For
        a	do
        Brasil,	há	240	mil	funcionários,	além	dos	890	mil	brasileiros	nas	instalações	de	Jacarezinh
        o	e
        nos	escritórios	em	todo	país.
    </p>
    <p>
        Dada	a	importância	econômica	da	empresa	para	o	Brasil,	a	família	Pelho	já	recebeu	diversos
        prêmios,
        homenagens	e	condecorações.	Todos	os	presidentes	do	Brasil	já	visitaram	as	instalações	da	M
        irror
        Fashion,	além	de	presidentes	da	União	Européia,	Ásia	e	o	secretário-geral	da	ONU.
    </p>


    <h2 id="diferenciais">Diferenciais</h2>
    <ul>
        <li> Menor preço do varejo, garantido</li>
        <li>Se você achar uma loja mais barata, leva o produto de graça</li>
        <li>Pague em reais, dólares, euros ou bitcoins</li>
        <li>Todas as compras com frete grátis para o mundo todo</li>
        <li>Maior comércio eletrônico de moda do mundo</li>
        <li>Atendimento via telefone, email, chat, twitter, facebook, carta, fax e telegrama</li>
        <li>Presente em 124 países</li>
        <li>Mais de um milhão de funcionários em todo o mundo</li>
    </ul>

    <div id="rodape">
        <img src="_imagens/logo.png">
        &copy; copyright Mirror Fashion
    </div>
</body>
>>>>>>> dev:CursoE-comerce/sobre.php
</html>